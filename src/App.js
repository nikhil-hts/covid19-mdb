import React from "react";
import { Grid } from "@material-ui/core";
import Header from "./Header";
import Content from "./Content";
import { makeStyles } from "@material-ui/core/styles";
import { DataProvider } from "./DataContext";
import Footer from "./Footer";
import NavInfo from "./NavInfo";
import Twitter from "./Twitter";
const useStyles = makeStyles({
  pos: {
    marginTop: 40,
  },
});
function App() {
  const classes = useStyles();
  return (
    <Grid container direction="column">
      <Grid item>
        <Header />
      </Grid>
      <Grid item container mt={12} className={classes.pos}>
        <Grid item xs={false} sm={false} />
        <Grid item xs={12} sm={12}>
          <NavInfo />
        </Grid>
      </Grid>
      <Grid item container mt={12} className={classes.pos}>
        <Grid item sm={2} />
        <Grid item xs={12} sm={8}>
          <DataProvider>
            <Content />
          </DataProvider>
        </Grid>
      </Grid>
      <Grid item container mt={12} className={classes.pos}>
        <Grid item xs={false} sm={3} />
        <Grid item xs={12} sm={6}>
          <Twitter />
        </Grid>
      </Grid>
      <Grid item container mt={12} className={classes.pos}>
        <Grid item xs={false} sm={false} />
        <Grid item xs={12} sm={12}>
          <Footer />
        </Grid>
      </Grid>
    </Grid>
  );
}

export default App;
