import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import { deepOrange } from "@material-ui/core/colors";
import Box from "@material-ui/core/Box";
const useStyles = makeStyles({
  root: {
    display: "flex",
    flexDirection: "column",
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
  },
  cardBox: {
    display: "flex",
    flexDirection: "column",
  },
  confirmed: {
    color: "#ff073a",
  },
  active: {
    color: "#007bff",
  },
  recovered: {
    color: "#28a745",
  },
  deaths: {
    color: "#6c757d",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 20,
  },
  pos: {
    marginBottom: 12,
  },
  orange: {
    backgroundColor: deepOrange[500],
  },
});
const SCard = (props) => {
  const classes = useStyles();
  const { title, avatar, date, confirmed, active, recovered, deceased } = props;
  return (
    <Card className={classes.root} spacing={6}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={classes.orange}>
            {avatar}
          </Avatar>
        }
        title={title}
        subheader={date}
      />
      <CardContent>
        <Box display="flex" px={1} mx={1}>
          <Box p={1} flexGrow={1}>
            <Typography
              className={(classes.title, classes.confirmed)}
              color="textSecondary"
              gutterBottom
            >
              Confirmed
            </Typography>
            <Typography variant="h5" component="h2">
              {confirmed}
            </Typography>
          </Box>
          <Box p={1}>
            <Typography
              className={(classes.title, classes.active)}
              color="textSecondary"
              gutterBottom
            >
              Active
            </Typography>
            <Typography variant="h5" component="h2">
              {active}
            </Typography>
          </Box>
        </Box>

        <Box display="flex" m={1} p={1}>
          <Box p={1} flexGrow={1}>
            <Typography
              className={(classes.title, classes.recovered)}
              color="textSecondary"
              gutterBottom
            >
              Recovered
            </Typography>
            <Typography variant="h5" component="h2">
              {recovered}
            </Typography>
          </Box>
          <Box p={1}>
            <Typography
              className={(classes.title, classes.deaths)}
              color="textSecondary"
              gutterBottom
            >
              Deceased
            </Typography>
            <Typography variant="h5" component="h2">
              {deceased}
            </Typography>
          </Box>
        </Box>
      </CardContent>
    </Card>
  );
};

export default SCard;
