import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import {
  TwitterTimelineEmbed,
  TwitterShareButton,
  TwitterFollowButton,
  TwitterHashtagButton,
  TwitterMentionButton,
  TwitterTweetEmbed,
  TwitterMomentShare,
  TwitterDMButton,
  TwitterVideoEmbed,
  TwitterOnAirButton,
} from "react-twitter-embed";
const useStyles = makeStyles({
  root: {
    display: "flex",
    flexDirection: "column",
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
  },
  cardBox: {
    display: "flex",
    flexDirection: "column",
  },
});
const Twitter = () => {
  const classes = useStyles();
  return (
    <Card className={classes.root} spacing={6}>
      <CardHeader title="Madhubani/Bihar Region Covid19 Updates" />
      <CardContent>
        <TwitterTimelineEmbed
          sourceType="profile"
          screenName="CovidMdbTracker"
          options={{ height: 600 }}
        />
      </CardContent>
    </Card>
  );
};

export default Twitter;
