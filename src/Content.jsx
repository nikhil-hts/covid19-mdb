import React, { useState, useContext } from "react";
import { Grid } from "@material-ui/core";
import Card from "./Card";
import Axios from "axios";
import { DataContext } from "./DataContext";
import { useEffectOnce } from "react-use";
import MomentDate from "./Moment";
const Content = () => {
  const [dswData, setDSWData] = useContext(DataContext);
  const [nationalData, setNationalData] = useState([]);
  const [stateData, setStateData] = useState({});
  //Use effect
  useEffectOnce(() => {
    fetchSW();
  });
  //All State data
  const fetchSW = async () => {
    const data = await Axios.get("https://api.covid19india.org/data.json");
    const allData = data.data.statewise;
    setNationalData(allData.find((one) => one.state === "Total"));
    setStateData(allData.find((one) => one.state === "Bihar"));
  };

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} sm={4}>
        <Card
          title={"India"}
          avatar={"I"}
          date={<MomentDate />}
          confirmed={nationalData.confirmed}
          active={nationalData.active}
          recovered={nationalData.recovered}
          deceased={nationalData.deaths}
        />
      </Grid>
      <Grid item xs={12} sm={4}>
        <Card
          title={"Bihar"}
          avatar={"B"}
          date={<MomentDate />}
          confirmed={stateData.confirmed}
          active={stateData.active}
          recovered={stateData.recovered}
          deceased={stateData.deaths}
        />
      </Grid>
      <Grid item xs={12} sm={4}>
        <Card
          title={"Madhubani"}
          avatar={"M"}
          date={<MomentDate />}
          confirmed={dswData.confirmed}
          active={dswData.active}
          recovered={dswData.recovered}
          deceased={dswData.deceased}
        />
      </Grid>
    </Grid>
  );
};
export default Content;
