import React from "react";
import Moment from "react-moment";

const MomentDate = () => {
  const dateToFormat = new Date();
  return <Moment format="D-MMM-YYYY">{dateToFormat}</Moment>;
};
export default MomentDate;
