import React, { useState, useEffect, createContext } from "react";

export const DataContext = createContext();

export const DataProvider = (props) => {
  const [dswData, setDSWData] = useState({});

  useEffect(() => {
    fetchDSW();
  }, []);

  //District wise data
  const fetchDSW = async () => {
    const data = await fetch(
      "https://api.covid19india.org/state_district_wise.json"
    );
    const items = await data.json();
    setDSWData(items.Bihar.districtData.Madhubani);
  };

  return (
    <DataContext.Provider value={[dswData, setDSWData]}>
      {props.children}
    </DataContext.Provider>
  );
};
